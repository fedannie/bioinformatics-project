#pragma once
spectrum GetSpectrum(const string& sequence);

int SharedPeaksCount(const spectrum& first, const spectrum& second);

void GetExperimentalSpectrums(const char*, vector <Replace> &replaces);
