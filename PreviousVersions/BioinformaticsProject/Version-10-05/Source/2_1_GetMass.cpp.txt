#include <iostream>
#include <string>
#include <fstream>
#include <sstream>
#include <map>                           
#include "2_0_AminoacidData.h"      
using namespace std;

const double EPS = 1e-10;
const char No = '0';
map <pair<char, char>, ld> Change;
map <char, double> Mass;
map <int, double> TheorMass; 

double CountMass(string s){
	double res = 0;	
	for(char p : s) res += Mass[p];
	return res + 18;
	cout << res << endl;
}
		
void GetMass(){   
	try{
		freopen("../IntermediateFiles/All", "r", stdin);
	}
	catch(...){
		cout << "Intermediate File All does not exist";
		return;
	}

	freopen("../IntermediateFiles/AllWithMass", "w", stdout);	
	

	stringstream ss;
	char s[10000], tag[100];
	int id = 0;
	bool flag = false;
	while (gets(s)){
		if (s[0] == 'I' && s[1] == 'd'){
			if (flag && TheorMass[id] > 0 + EPS) cout << ss.str();
			ss.str("");
			ss.clear();
			sscanf(s, "Id= %d Tag= %s", &id, tag);
			ss << s << endl << TheorMass[id] << endl;
		} else {
			flag = true;
			if (s[0] != 's') ss << s << ' ' << CountMass(s) << endl;
			else ss << s << endl;
		}
	}
	if (TheorMass[id] != 0) cout << ss.str();
	fclose(stdin);
	fclose(stdout);

}
	                            
