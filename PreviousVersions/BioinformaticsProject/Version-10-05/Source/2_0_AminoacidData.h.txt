#ifndef _AMNOACID_DATA_H
#define _AMNOACID_DATA_H

#include <map>
#include <algorithm>
using namespace std;
#define mp make_pair
#define ld double
#define fst first
#define snd second
#define pb push_back
#define amino pair< pair <char, char>, ld>

struct triplet{
	char fst, snd, thrd;	
	bool operator<(const triplet &other) const{
		return ((fst < other.fst) || (fst == other.fst && snd < other.snd) || (fst == other.fst && snd == other.snd && thrd < other.thrd));
	}
};
                
extern const char No;
        
struct answer{
	char fr[3];
	char to[3];
	void make(){
		fr[0] = No, to[0] = No, fr[1] = No, to[1] = No, fr[2] = No, to[2] = No;
	}
};

extern map <pair<char, char>, ld> Change;
extern map <char, double> Mass;
extern map <int, double> TheorMass;       
void SetData();

#endif