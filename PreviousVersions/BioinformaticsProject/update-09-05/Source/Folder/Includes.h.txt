#pragma once
#include <map>
#include <vector>
#include <string>
#include <cstring>
#include <algorithm>
#include "FastInput.h"
#include "FastMemoryAllocation.h"
const int max_peptide_name_length = 1000, max_peptide_length = 200000;

#define pb push_back
#define mp make_pair
#define fst first
#define snd second
