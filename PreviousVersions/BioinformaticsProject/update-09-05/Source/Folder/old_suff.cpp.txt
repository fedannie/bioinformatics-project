
int q[2 * max_peptide_length]; //for queue in SuffAut

template <const int V, const int E>
struct MultiList{
	int e, head[V], next[E + 1], to[E + 1];
	MultiList(size_t sz = sizeof(head)){ clear(sz); }
	void clear(size_t sz = sizeof(head)){ e = 0; memset(head, -1, sz); }

	void add(int v, int x){
//		assert(e <= E);
		to[e] = x;
		next[e] = head[v], head[v] = e++;
	}
};

MultiList <2 * max_peptide_length, 3 * max_peptide_length> inv_links;


class SuffAut{
private:
	struct node{
		bool is_clone;
		int link, len, first_pos;

		int next[26]; //can try just array or list or both in different cases. Sergey says that usually array is the best
	};				//but if you need to build it for many strings it'll be slow to fill it with zeroes
	
	node* st;
	int sz;
	
	char* old_mem_pos;
public:	
	int op;
	SuffAut(const char* s){ //if you don't need inverted links - just delete
		op = 0;
		old_mem_pos = mem_pos;

		int lll = 2 * strlen(s);
		st = new node[lll];

		memset(st, 0, lll * sizeof(node));
		
		int last = 0; sz = 1;
		st[0].link = -1;
		
		char c;
		for(; *s; ++s){
			int cur = sz++, p; c = *s - 'A';
			st[cur].len = st[last].len + 1, st[cur].first_pos = st[last].len;
			
			for(p = last; p != -1 && !st[p].next[*s]; p = st[p].link)
				st[p].next[*s] = cur;
 
			if (p != -1){
				int q = st[p].next[*s];
				if (st[p].len + 1 == st[q].len) st[cur].link = q;
				else{
					int clone = sz++;
					memcpy(st + clone, st + q, sizeof(node));
					st[clone].len = st[p].len + 1;
				
					for (; p != -1 && st[p].next[*s] == q; p = st[p].link)
						st[p].next[*s] = clone;
					st[q].link = st[cur].link = clone;
					st[clone].is_clone = true;
				}
			}
			last = cur;
		}

		for(int i = 1; i < sz; i++) inv_links.add(st[i].link, i);
	}

	vector <int> GetAllOccurrences(const char* s, int len){
//		cerr << "in" << endl;
		int u = 0;
		vector <int> ans;

		for(;*s;++s) if (!(u = st[u].next[*s - 'A'])){// cerr << "out" << endl;
	return ans;}

		q[0] = u;
		int *l = q, *r = q + 1;
		while (r > l){
			u = *l, l++; op++;
			if (!st[u].is_clone) ans.pb(st[u].first_pos - len + 1);
			for(int e = inv_links.head[u]; e != -1; e = inv_links.next[e]) *r = inv_links.to[e], ++r, op++;
		}

		return ans;
	}
				
				
	~SuffAut(){ mem_pos = old_mem_pos; inv_links.clear(sz * sizeof(int)); } //if you have your own allocator and care about memory
	
};	

