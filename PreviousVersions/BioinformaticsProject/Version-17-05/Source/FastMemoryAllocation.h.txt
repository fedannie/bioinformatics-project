#pragma once
extern char mem[1 << 28];
extern char* mem_pos;

inline void* operator new(size_t size){
	void* res = mem_pos; mem_pos += size;
	return res;
}

inline void* operator new[](size_t size){
	void* res = mem_pos; mem_pos += size;
	return res;
}


inline void operator delete(void* ){}
inline void operator delete[](void* ){}
