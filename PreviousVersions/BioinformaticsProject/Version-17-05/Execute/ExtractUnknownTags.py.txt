import re, sys

All = []
with open(sys.argv[1]) as f:
	for line in f:
		All.append( [line.split()[1], line.split()[2]] )

DetectedIds = {}
with open(sys.argv[2]) as f:
	f.readline()
	for line in f:
		scan = re.sub('scan=', "", line.split()[1])
		DetectedIds[int(scan)] = ""


ans = []
num = 0
with open(sys.argv[3], 'w') as f:
	for i in All:
		if DetectedIds.get( int(i[0]) ) == None:
			ans.append(i[0] + ' ' + i[1] + '\n')
			num += 1
	f.write(str(num) + '\n')
	for i in ans: f.write(i)

