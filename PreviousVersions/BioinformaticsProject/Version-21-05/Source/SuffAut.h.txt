class SuffAut{
private:
	int sz;
	
public:	
	SuffAut(const char* s);

	vector <int> GetAllOccurrences(const char* s, int len);
	~SuffAut();
};	
