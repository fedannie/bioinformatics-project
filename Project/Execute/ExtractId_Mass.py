import re, sys
M = {}
s = ' '
Eps = 0.0000000000001
with  open(sys.argv[1]) as f:
	while True:
		while s and s != 'BEGIN IONS\n': s = f.readline()
		if not s:  break

		Id = int(re.search(r'[0-9.-]+', f.readline()).group(0))

		while re.match('PRECURSOR_MASS', s) == None: s = f.readline()
		Mass = float(re.search(r'[0-9.-]+', s).group(0))

		M[Id] = Mass;
		while s != 'END IONS\n':	
			s = f.readline()

	
for i in M.items():	
	if i[1] > 0.0 + Eps:
		print(i[0], i[1])

