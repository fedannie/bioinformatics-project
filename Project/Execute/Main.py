import os, re

def CheckError(name, path):
	if path == '': 
		print("No path to file " + path)
		exit(0)
	try:
		f = open(path)
	except FileNotFoundError:
		print(path + ' not found')
		exit(0)
	else: f.close()


files = [ ['Experiment', ''], ['DataBase', ''], ['SpectrumContributions', ''], ['Recognized', ''] ]

with open("../PathsToData") as f:
	for s in map(lambda x : x.split(), f.readlines()):
		for x in files:
			if re.match(x[0], s[0]): x[1] = s[1]

for x in files:	CheckError(x[0], x[1])


	
print("Checked input files")

print("Extracting unknown tags...")
os.system(r'python ExtractUnknownTags.py ' + files[2][1] + ' ' + files[3][1] + '  ../IntermediateFiles/UnknownTags')

print("Extracting Ids and masses...")
os.system(r'python ExtractId_Mass.py ' + files[0][1] + ' > ../IntermediateFiles/Id_Mass')

os.system(r'CheckMassDifference.exe ' + files[0][1] + " " + files[1][1])



 

