using namespace std;
#include "Defines.h"
#include "Includes.h"
#include "Data.h"
#include "Replace.h"

double CountMass(string s){
	double res = 0;	
	for(char p : s){
		try{ res += Mass[aminoacid(p)]; }
		catch(const char* err){ cerr << err << " in string " << s << endl; }
	}			
	return res + H2O_Mass;
}
		
void GetMass(vector <Replace>& pept){   
	for (auto &x : pept)
		for (auto &one : x.tags){
			one.experiment = ExperimentalMass[one.id];
			one.theory = CountMass(one.expanded);
		}
}
	                            
