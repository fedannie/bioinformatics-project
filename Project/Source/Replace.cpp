using namespace std;
#include "Includes.h"
#include "Defines.h"
#include "Data.h"
#include "Replace.h"
#include "3_1_Spectra.h"

Infoexpanded :: Infoexpanded(){}
Infoexpanded :: Infoexpanded(int _id, string _tag, string _expanded) : id(_id), tag(_tag), expanded(_expanded) {}

void Infoexpanded :: Print(FILE* out, map <ld, string> &Best){
        fprintf(out, "Id = %d Tag = %s ExpandedTag = %s\n", id, tag.c_str(), expanded.c_str());
        fprintf(out, "%Lf %Lf\n", theory, experiment);
        fprintf(out, "%d\n", rep.size());

        char from, to;
        char inf[10000];
	for (aminodelta &x : rep){
		from = x.fst.p.fst.Val(), to = x.fst.p.snd.Val();
		possible_changes[from - 'A'].push_back(to);

		sprintf(inf, "%s %c %c MassDiff= %Lf SharedPeaksCount= %d Probability= %.8Lf\n", expanded.c_str(), from, to, x.snd, GetBestSharedPeaksCount(from, to), ChangeProbability[x.fst]);		
		Best.insert( mp(abs(x.snd), string(inf)) );
		
		fprintf(out, "%c %c MassDiff= %Lf SharedPeaksCount= %d Probability= %.8Lf\n", from, to, x.snd, GetBestSharedPeaksCount(from, to), ChangeProbability[x.fst]);		
	}
	fprintf(out, "\nMutated Forms:\n");
	for (size_t i = 0; i < expanded.size(); i++){
		char amino = expanded[i];
		if (amino == 'X' || possible_changes[amino - 'A'].size() > 0)
			for (char ch : possible_changes[amino - 'A']){
				string str = expanded;
				str[i] = ch;
				fprintf(out, "%s\n", str.c_str());
			}
	}
	fprintf(out, "\n\n\n");
}

int Infoexpanded :: GetBestSharedPeaksCount(char from, char to){
	string new_tag(expanded);

	int best = 0;
	for(char &c : new_tag)	
		if (c == from){
			c = to;
			best = max(best, SharedPeaksCount(ExperimentalSpectrum, GetSpectrum(new_tag)));
			c = from;
		}

	return best;
}	


Replace :: Replace(){}

Replace :: Replace(string name) : information(name) {}


void Replace :: add(int _id, string _tag, string _expanded){ tags.pb( Infoexpanded(_id, _tag, _expanded) ); }

void Replace :: Print(FILE* out, map <ld, string>& Best){
	fprintf(out, "%s\n", information.c_str());
	for (Infoexpanded &x : tags)
		x.Print(out, Best);
}