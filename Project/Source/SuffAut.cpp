using namespace std;

#include "Defines.h"
#include "Includes.h"
#include "SuffAut.h"
#include "Data.h"

static int q[2 * max_peptide_length]; //for queue in SuffAut

template <const int V, const int E>
struct MultiList{
	int e, head[V], next[E + 1], to[E + 1];
	MultiList(size_t sz = sizeof(head)){ clear(sz); }
	void clear(size_t sz = sizeof(head)){ e = 0; memset(head, -1, sz); }

	void add(int v, int x){
//		assert(e <= E);
		to[e] = x;
		next[e] = head[v], head[v] = e++;
	}
};

static MultiList <2 * max_peptide_length, 3 * max_peptide_length> inv_links;


struct node{
	bool is_clone;
	int link, len, first_pos;
	
	int next[26];
};		
		

static node st[2 * max_peptide_length];

	SuffAut :: SuffAut(const char* s){
		size_t slen = 2 * strlen(s);
		memset(st, 0, slen * sizeof(node));
		
		int last = 0; sz = 1;
		st[0].link = -1;
		
		for(; *s; ++s){
			int cur = sz++, p, c = *s - 'A';
			st[cur].len = st[last].len + 1, st[cur].first_pos = st[last].len;
			
			for(p = last; p != -1 && !st[p].next[c]; p = st[p].link)
				st[p].next[c] = cur;
 
			if (p != -1){
				int q = st[p].next[c];
				if (st[p].len + 1 == st[q].len) st[cur].link = q;
				else{
					int clone = sz++;
					memcpy(st + clone, st + q, sizeof(node));
					st[clone].len = st[p].len + 1;
				
					for (; p != -1 && st[p].next[c] == q; p = st[p].link)
						st[p].next[c] = clone;
					st[q].link = st[cur].link = clone;
					st[clone].is_clone = true;
				}
			}
			last = cur;
		}

		for(int i = 1; i < sz; i++) inv_links.add(st[i].link, i);
	}

	vector <int> SuffAut :: GetAllOccurrences(const char* s, int len){
		int u = 0;
		vector <int> ans;

		for(;*s;++s) if (!(u = st[u].next[*s - 'A'])) return ans;

		q[0] = u;
		int *l = q, *r = q + 1;
		while (r > l){
			u = *l, l++;
			if (!st[u].is_clone) ans.pb(st[u].first_pos - len + 1);
			for(int e = inv_links.head[u]; e != -1; e = inv_links.next[e]) *r = inv_links.to[e], ++r;
		}

		return ans;
	}
				
				
	SuffAut :: ~SuffAut(){ inv_links.clear(sz * sizeof(int)); }

