#pragma once
spectrum GetSpectrum(const string& sequence);

int SharedPeaksCount(const spectrum& first, const spectrum& second);

void GetExperimentalSpectra(const char*, vector <Replace> &replaces);
