using namespace std;
#include "Defines.h"
#include "Includes.h"
#include "SuffAut.h"
#include "Data.h"
#include "Replace.h"
#include "FastInput.h"

int CountGaps(int *pos, const char* s){
	int cnt = 1, cur = 1; *pos = -1;
	++s;
	while (*s){
		if (*s == 'R' || (*s == 'K' && *(s + 1) != 'P')) pos[cnt++] = cur;
		++cur, ++s;
	}
	if (pos[cnt - 1] != cur - 1) pos[cnt++] = cur - 1;
	return cnt;
}

vector <Replace>* ProcessTags(vector < pair<string, int> > &tags){
	char PeptideName[max_peptide_name_length], peptide[max_peptide_length];
	int gap_pos[max_peptide_length], left, right;
	
	PeptideName[0] = '>'; 
	vector <Replace> *reps = new vector <Replace>;
	getChar(); //omit first >
	while (true){
		read_str(PeptideName + 1, '\n'); if (*(PeptideName + 1) == 0) break; //end of file
	        read_str(peptide, '>');
		
		int num = CountGaps(gap_pos, peptide);
		SuffAut *aut = new SuffAut(peptide);
 
		vector <int> occur;
		bool put_name = false;
		for (auto &t : tags){
			occur = aut->GetAllOccurrences(t.fst.c_str(), t.fst.size());
	
			if (occur.size() && !put_name)
				reps->pb( Replace(PeptideName) ), put_name = true;
		
			for(auto pos : occur){
				left = *(lower_bound(gap_pos, gap_pos + num, pos) - 1) + 1;
				right = *(lower_bound(gap_pos, gap_pos + num, pos + (int) t.fst.size() - 1));

				reps->back().add(t.snd, t.fst, string(peptide, left, right - left + 1) );
				
			}
		}
		delete aut;
	}
	return reps;
}
