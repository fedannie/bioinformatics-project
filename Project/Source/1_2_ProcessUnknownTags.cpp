using namespace std;
#include "Includes.h"
#include "Defines.h"
#include "Data.h"
#include "Replace.h"
#include "1_0_ProcessTag.h"

void ReadTags(vector < pair<string, int> > &tags){
	size_t num;
	cin >> num;

	tags.resize(num);
	for(size_t i = 0; i < num; i++)
		 cin >> tags[i].snd >> tags[i].fst;
}

vector <Replace>& ProcessUnknownTags(const char* DataFile){
	if (!freopen("../IntermediateFiles/UnknownTags","r", stdin)){ cerr << "Intermediate files UnknownTags does not exist"; exit(0);}
	cin.clear();
		
	vector < pair<string, int> > tags;
	ReadTags(tags);
	freopen(DataFile, "r",stdin);
	return *ProcessTags(tags);
}	 
		
		