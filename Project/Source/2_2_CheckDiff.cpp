using namespace std;
#include "Includes.h"
#include "Defines.h"
#include "Data.h"
#include "Replace.h"
#include "FastInput.h"  

vector <aminodelta> Closest(ld a, ld b){
	vector <aminodelta> ans;
	ld delta = b / 1000000 * PPM;
        for(auto &x : Change)
		if (abs(a - b - x.snd) < delta + EPS && ChangeProbability[x.fst.p] > EPS)
			ans.pb( mp(x.fst, a - b - x.snd) );		
	return ans;
}

bool comp(const aminodelta a, const aminodelta b) {
	return a.snd < b.snd;
}

vector <Replace>& CheckDiff(vector <Replace>& reps){
	vector <Replace> *ans = new vector <Replace>;
	for (auto pept : reps){
		Replace cur = pept;
		cur.tags.clear();
		for (auto next : pept.tags){
			if (next.theory > 0 + EPS){
				vector <aminodelta> tmp = Closest(next.theory, next.experiment);

				if (next.expanded.find('X') != string :: npos) next.xflag = true;
				sort(tmp.begin(), tmp.end(), comp);
				if (tmp.size() > 0)
					for (auto x : tmp)
						if (next.xflag || next.expanded.find(x.fst.p.fst.Val()) != string :: npos) 
							next.rep.push_back(x);
			}
			if (next.rep.size() > 0) 
				cur.tags.push_back(next);
		}
		if (cur.tags.size() > 0) 
			ans->push_back(cur);
	}
	return *ans;
}