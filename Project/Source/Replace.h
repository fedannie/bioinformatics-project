#pragma once
struct Infoexpanded{
        Infoexpanded();
        Infoexpanded(int, string, string);
       	int id;
	string tag, expanded;
	vector <aminodelta> rep;
	vector<char> possible_changes[26];
	unordered_set<string> new_peptides;
	ld theory = 0, experiment = 0;	
	spectrum ExperimentalSpectrum;
	bool xflag = false;
	void Print(FILE*, map <ld, string>&);
	int GetBestSharedPeaksCount(char, char);		
};

struct Replace{
	string information;
	vector <Infoexpanded> tags;
	Replace();
	Replace(string);
	void Print(FILE*, map <ld, string>&);
	void add(int, string, string);
};
