using namespace std;
#include "Defines.h"
#include "Includes.h"
#include "Data.h"
#include "Replace.h"
#include "2_1_GetMass.h" 
#include "FastInput.h"
#include "Format.h"

spectrum GetSpectrum(const string& sequence){
	spectrum res, final;	
	if (sequence.size() == 0) return res;

	double suff = CountMass(sequence), pref = 0;
	for(char c : sequence){
		pref += Mass[c], suff -= Mass[c];
		res.pb(pref), res.pb(suff);
	}

	res.pop_back(); //delete empty suffix ... !whole string is counted!
	
	sort(res.begin(), res.end());
	final.pb(res.front());
	for(auto peak : res)
		if (abs(peak - final.back()) > spectrum_mistake) final.pb(peak);

	return final;
}

int SharedPeaksCount(const spectrum& _first, const spectrum& _second){
	auto first = _first, second = _second;
	sort(first.begin(), first.end()), sort(second.begin(), second.end());

	size_t shared_peaks = 0, i = 0, j = 0;
	while (i < first.size() && j < second.size()){
		if (abs(first[i] - second[j]) < spectrum_mistake) ++shared_peaks;
		if (first[i] < second[j]) ++i;
		else ++j;
	}

	return shared_peaks;
}


void GetExperimentalSpectra(const char* file, vector <Replace> &replaces){
	FILE* f = freopen(file, "r", stdin);
	if (f == NULL){ cerr << "\nfile " << file << " not found"; exit(0); }
	
	const int max_string_length = 100000;

	char s[max_string_length]; bool NEOF = true; int id;
	while ((NEOF = read_str(s, '\n'))){
		while ((id = GetScan(s)) == -1 && (NEOF = read_str(s, '\n')));
		if (!NEOF) break;
 
		do{ read_str(s, '\n'); }
		while (!isdigit(*s));

		spectrum spectr; ld peak;
		while (isdigit(*s)) sscanf(s, "%Lf", &peak), read_str(s, '\n'), spectr.pb(peak);
		
		for(auto &rep : replaces)
			for(auto &tag : rep.tags)
				if (tag.id == id) tag.ExperimentalSpectrum = spectr;
	}

}			
						