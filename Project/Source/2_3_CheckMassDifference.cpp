using namespace std;
#include "Includes.h"
#include "Defines.h"
#include "Data.h"               
#include "Replace.h"
#include "Format.h"
#include "2_1_GetMass.h"
#include "2_2_CheckDiff.h"
#include "3_1_Spectra.h"
#include "1_2_ProcessUnknownTags.h"

void PrintResult(vector <Replace>& replaces){
		FILE* out = fopen("../Result", "w");
		fprintf(out, "%s", ResultFormat);


		map <ld, string> Best;
		for (auto &x : replaces) x.Print(out, Best);
		fclose(out);

		ofstream out_best("../BestReplaces");
		for(auto x : Best) out_best << x.snd;
		out_best.close();
}		

void PrintAllPeptides(vector <Replace>& replaces){
		set <string> Peptides;
		for (auto &x : replaces) 
			for (auto &y : x.tags)
			        if (!Peptides.count(y.expanded)) Peptides.insert(y.expanded);

		ofstream out("../peptides");
		for(auto p : Peptides) out << p << endl;
		out.close();
}	

int main(int argc, char* argv[]){
	try{
	        cout << "Setting Data..." << endl;
        	SetData(); 

		cout << "Expanding unknown tags..." << endl;
		auto reps = ProcessUnknownTags(argv[2]);

		cout << "Getting theoretical masses..." << endl;
		GetMass(reps);

		cout << "Finding replaces..." << endl;
		auto replaces = CheckDiff(reps); 

		cout << "Getting experimental masses...";
		GetExperimentalSpectra(argv[1], replaces);
		
		PrintResult(replaces);
		
		PrintAllPeptides(replaces);		
		cout << "All done";
	}
	catch(const char *error){ cerr << error; exit(0); }
	
	return 0;	
}
	