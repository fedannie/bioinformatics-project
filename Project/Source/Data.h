#ifndef _AMINOACID_DATA_H
#define _AMINOACID_DATA_H

void CheckAmino(char val);

class aminoacid{
	private:
		char val;
	public:
		aminoacid(char ch);
		aminoacid();
		~aminoacid();
		aminoacid operator= (char ch);
		bool operator== (const aminoacid &a) const;
		char Val() const;
		char Val();
};                      

bool operator< (const aminoacid &a, const aminoacid &b);  

class aminopair{
	public:
		pair <aminoacid, aminoacid> p;
		aminopair(aminoacid a, aminoacid b);
		aminopair(const pair <aminoacid, aminoacid> &x);
		~aminopair();
};                          

bool operator< (const aminopair &a, const aminopair &b);

typedef pair <aminopair, ld> aminodelta;
typedef vector <ld> spectrum;


extern map <aminopair, ld> Change;
extern map <aminoacid, ld> Mass;
extern map <int, double> ExperimentalMass;       
extern map <aminopair, ld> ChangeProbability;
extern int PPM;
void SetData();

const int AminoAcidsCount = 20;
const double spectrum_mistake = 1e-2; //distance at wich spectrums are considered to be equal
const long double EPS = 0.00000000001;
const long double H2O_Mass = 18.01057;
const int max_peptide_name_length = 10000, max_peptide_length = 200000, max_tag_size = 1000, max_expanded_tag_size = 100000;




#endif


