using namespace std;
#include "Includes.h"

int GetScan(const char* s){ return strstr(s, "SCANS=") == s ? atoi(s + 6) : -1; }

int GetId(const string &s){
	if (s.find("ID=") == 0) return atoi(s.c_str() + 5);
	else throw "Bad format";
}
