#ifndef _AMINOS_
#define _AMINOS_
using namespace std;
#include "Defines.h"
#include "Includes.h"
#include "Data.h"

void CheckAmino(char val){
	if (!(val >= 'A' && val <= 'Y' && val != 'B' && val != 'J' && val != 'O' && val != 'U')) throw "incorrect aminoacid";
}                                                                                                     
aminoacid::aminoacid(char ch){
	try{
		CheckAmino(ch);
		val = ch;
	}
	catch (const char *s) { throw s; }
}

aminoacid::aminoacid(){}

aminoacid::~aminoacid(){}

aminoacid aminoacid::operator= (char ch){
	try{
		CheckAmino(ch);
		val = ch;
	}
	catch (const char *s) { throw s; }
	return (*this);
}

char aminoacid::Val() const{
	return val;
}
char aminoacid::Val(){
	return val;
}


bool operator< (const aminoacid &a, const aminoacid &b){
	return a.Val() < b.Val();
}
bool operator< (const aminopair &a, const aminopair &b){
	return (a.p.fst < b.p.fst || (a.p.fst == b.p.fst && a.p.snd < b.p.snd));
}
                                  
bool aminoacid::operator== (const aminoacid &a) const{
	return val == a.Val();
}                         

aminopair::aminopair (aminoacid a, aminoacid b){
	p.fst = a;
	p.snd = b;
} 
aminopair::aminopair (const pair <aminoacid, aminoacid> &x) : p(x) {}

aminopair::~aminopair (){} 

void SetData(){
	FILE *in_theory, *in_masses, *in_mass_difference, *in_probability, *in_configuration;
	if (!(in_theory = fopen("../IntermediateFiles/Id_Mass", "r")) ){
		cerr << "Problems with opening intermediate file 'Id_Mass'"; goto close;
	}		
	
	if (!(in_masses = fopen("../Data/Masses", "r")) ){
		cerr << "Problems with opening data file 'Masses'"; goto close;	
	}

	if (!(in_mass_difference = fopen("../Data/MassDifference", "r")) ){
		cerr << "Problems with opening data file 'MassDifference'"; goto close;	
	}

	if (!(in_probability = fopen("../Data/ChangeProbability", "r")) ){
		cerr << "Problems with opening data file 'ChangeProbability'"; goto close; 
	}
	
	if (!(in_configuration = fopen("../Data/configuration", "r")) ){
		cerr << "Problems with opening data file 'configuration'"; goto close; 
	}

	if (fscanf(in_configuration, "PPM= %d", &PPM) == -1){ cerr << "Cannot find PPM in file 'configuration'"; goto close; }
	
	int Id;	ld exper_mass;
	while( fscanf(in_theory, "%d %Lf\n", &Id, &exper_mass) != -1) ExperimentalMass[Id] = exper_mass;        
	
	char acid; ld mass;
	while (fscanf(in_masses, "%c %Lf\n", &acid, &mass) != -1) Mass[aminoacid(acid)] = mass;
	
	char from, to; ld difference;
	while (fscanf(in_mass_difference, "%c%c %Lf\n", &from, &to, &difference) != -1) Change[ mkap(aminoacid(from), aminoacid(to)) ] = difference;
	
	
	char Acid[AminoAcidsCount];
	for(size_t i = 0; i < AminoAcidsCount; ++i) fscanf(in_probability, "%c", &Acid[i]);

	for(size_t i = 0; i < AminoAcidsCount; ++i)
		for(size_t j = 0; j < AminoAcidsCount; ++j) 
			fscanf(in_probability, "%Lf", &ChangeProbability[ aminopair(Acid[i], Acid[j])]);
			
close:	fclose(in_theory), fclose(in_masses), fclose(in_mass_difference), fclose(in_probability), fclose(in_configuration);			
}
#endif
